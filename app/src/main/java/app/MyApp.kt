package app

import android.app.Application

class MyApp : Application() {

    companion object {
        const val DEFAULT_ERROR_TAG = "scanner_error";
        const val DEFAULT_INFO_TAG = "scanner_info";
    }

    override fun onCreate() {
        super.onCreate()
    }

}