package util

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.MyApp


fun Any.logError(initial: String? = null, tag: String = MyApp.DEFAULT_ERROR_TAG) {
    Log.e(tag, (if (initial != null) "$initial: " else "") + this.toString())
}

fun Any.logInfo(initial: String? = null, tag: String = MyApp.DEFAULT_ERROR_TAG) {
    Log.i(tag, (if (initial != null) "$initial: " else "") + this.toString())
}


fun AppCompatActivity.popToast(message: String? = null) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Any.getTag() = this.javaClass.name