package pojo.res

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class MyResponse(
    @SerializedName("criteria")
    var criteria: ArrayList<Criteria>? = null,
    @SerializedName("color")
    var color: String? = null,
    @SerializedName("tag")
    var tag: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("id")
    var id: Int = 0
):Parcelable
