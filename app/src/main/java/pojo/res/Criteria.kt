package pojo.res

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import java.util.*

@Parcelize
data class Criteria(
    @SerializedName("text")
    var text: String? = null,
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("variable")
    var variable: @RawValue HashMap<String, Variable>? = null
) : Parcelable
