package pojo.res

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Variable(

    @SerializedName("values")
    var values: ArrayList<Float>? = null,

    @SerializedName("type")
    var type: String? = null,

    @SerializedName("default_value")
    var default_value: Int = 0,

    @SerializedName("max_value")
    var max_value: Int = 0,

    @SerializedName("min_value")
    var min_value: Int = 0,

    @SerializedName("parameter_name")
    var parameter_name: String? = null,

    @SerializedName("study_type")
    var study_type: String? = null
) : Parcelable
