package api

class Constant {

    companion object {

        const val BASE_URL = "https://mp-android-challenge.herokuapp.com";
        const val DATA_URL = "/data"
        const val CURRENT_DATA = "current_data"
        const val CRITERIA = "criteria"
        const val VARIABLE = "variable"
    }
}