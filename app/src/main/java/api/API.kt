package api

import pojo.res.MyResponse
import retrofit2.http.GET

interface API {

    @GET(Constant.DATA_URL)
    suspend fun getData(): List<MyResponse>

}
