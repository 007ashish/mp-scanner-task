package ui.fragment.criteria

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import api.Constant
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.CriteriaListLayoutBinding
import pojo.res.Criteria
import pojo.res.Variable
import util.logError

class CriteriaAdapter(var criteria: List<Criteria>?) :
    RecyclerView.Adapter<CriteriaAdapter.CriteriaViewHolder>() {
    private var layoutInflater: LayoutInflater? = null

    private val mSpannableFactory = object : Spannable.Factory() {
        override fun newSpannable(source: CharSequence): Spannable {
            return super.newSpannable(source)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CriteriaViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<CriteriaListLayoutBinding>(
            layoutInflater!!,
            R.layout.criteria_list_layout,
            parent,
            false
        )

        return CriteriaViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CriteriaViewHolder, position: Int) {
        try {
            holder.binding.tvText.setText(
                criteria?.get(position)?.let { replaceVarWithVal(it) },
                TextView.BufferType.SPANNABLE
            )
            criteria?.get(position)?.let { setHighLightedText(holder.binding.tvText, it) }
        } catch (e: Exception) {
            e.logError()
        }

    }

    private fun setHighLightedText(tvText: TextView, criteria: Criteria) {
        val source = tvText.text.toString()
        var spannable = tvText.text as Spannable
        spannable = SpannableString(source)

        if (criteria.variable != null) {
            for (variable in criteria.variable!!.values) {
                val value = getVarValue(variable)
                val startIndex = source.indexOf(value)
                val endIndex = startIndex + value.length

                spannable.setSpan(
                    getClickableSpans(variable),
                    startIndex,
                    endIndex,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                tvText.setText(spannable, TextView.BufferType.SPANNABLE)
            }
        }
    }

    private fun replaceVarWithVal(criteria: Criteria): String? {
        var source = criteria.text
        if (criteria.variable != null) {
            for ((currentVarKey, variable) in criteria.variable!!) {
                source = source!!.replace(currentVarKey, getVarValue(variable))
            }
        }
        return source
    }

    private fun getVarValue(variable: Variable): String {
        val value: String
        if (variable.values != null) {
            value = variable.values!![0].toString()
        } else {
            value = variable.default_value.toString()
        }
        return "($value)"
    }

    private fun getClickableSpans(variable: Variable): ClickableSpan {
        val bundle = Bundle()
        bundle.putParcelable(Constant.VARIABLE, variable)
        return object : ClickableSpan() {
            override fun onClick(widget: View) {
                if (variable.values != null) {
                    Navigation.findNavController(widget).navigate(R.id.destinationValues, bundle)
                } else {
                    Navigation.findNavController(widget).navigate(R.id.destinationIndicator, bundle)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return criteria?.size ?: 0
    }

    fun setData(criteria: List<Criteria>) {
        this.criteria = criteria
        notifyDataSetChanged()
    }

    inner class CriteriaViewHolder(var binding: CriteriaListLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            binding.tvText.setSpannableFactory(mSpannableFactory)
            binding.tvText.movementMethod = LinkMovementMethod.getInstance()
        }

    }


}
