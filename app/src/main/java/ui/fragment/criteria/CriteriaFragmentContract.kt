package ui.fragment.criteria

import pojo.res.Criteria

interface CriteriaFragmentContract {

    interface View {
        fun initView()

        fun initRecyclerView(criteriaList: List<Criteria>)

    }

    interface ViewModel

    interface Repo

}
