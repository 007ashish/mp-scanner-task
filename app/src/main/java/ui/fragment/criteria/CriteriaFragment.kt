package ui.fragment.criteria


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import api.Constant
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.FragmentCriteriaBinding
import pojo.res.Criteria
import pojo.res.MyResponse
import java.util.*


class CriteriaFragment : Fragment(), CriteriaFragmentContract.View {


    private var rootView: View? = null
    private var binding: FragmentCriteriaBinding? = null
    private var data: MyResponse? = null
    private var criteriaList = ArrayList<Criteria>()
    private var criteriaAdapter: CriteriaAdapter? = null
    private var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_criteria, container, false)
        rootView = binding!!.root

        if (arguments != null) {
            data = arguments?.getParcelable(Constant.CURRENT_DATA)
            criteriaList = arguments?.getParcelableArrayList(Constant.CRITERIA)!!
            binding!!.data = data
            binding!!.color = Color()
            initRecyclerView(criteriaList)
        }

        return rootView
    }

    override fun initView() {

    }


    override fun initRecyclerView(criteriaList: List<Criteria>) {
        val recyclerView = binding!!.rvCriteria
        criteriaAdapter = CriteriaAdapter(criteriaList)
        val linearLayout = LinearLayoutManager(mContext)
        recyclerView.layoutManager = linearLayout
        recyclerView.adapter = criteriaAdapter
    }

}
