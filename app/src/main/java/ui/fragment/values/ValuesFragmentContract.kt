package ui.fragment.values

interface ValuesFragmentContract {

    interface View {
        fun initView()
        fun initRecyclerView()
    }

    interface ViewModel {}

    interface Repo {}
}
