package ui.fragment.values

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.ValuesListLayoutBinding
import util.logError
import java.util.*

class ValuesAdapter() : RecyclerView.Adapter<ValuesAdapter.ValuesViewHolder>() {
    private val values = ArrayList<Float>()
    private var layoutInflater: LayoutInflater ? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ValuesViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ValuesListLayoutBinding>(layoutInflater!!, R.layout.values_list_layout, parent, false)

        return ValuesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ValuesViewHolder, position: Int) {
        try {
            holder.binding.setValues(values[position])
        } catch (e: Exception) {
            e.logError()
        }

    }

    override fun getItemCount(): Int {
        return values.size
    }

    fun setData(values: List<Float>) {
        this.values.addAll(values)
        notifyDataSetChanged()
    }

     class ValuesViewHolder(var binding: ValuesListLayoutBinding) :
        RecyclerView.ViewHolder(binding.getRoot()) {

        init {
            binding.tvText.setMovementMethod(LinkMovementMethod.getInstance())
        }

    }
}
