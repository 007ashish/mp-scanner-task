package ui.fragment.values


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import api.Constant
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.FragmentValuesBinding
import pojo.res.Variable


class ValuesFragment : Fragment(), ValuesFragmentContract.View {

    lateinit var binding: FragmentValuesBinding
    lateinit var rootView: View
    private var valuesAdapter: ValuesAdapter? = null
    private var variable: Variable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            variable = arguments!!.getParcelable<Variable>(Constant.VARIABLE)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_values, container, false)
        rootView = binding.getRoot()

        initView()
        initRecyclerView()

        return rootView
    }

    override fun initView() {

    }

    override fun initRecyclerView() {
        val recyclerView = binding.rvValues
        valuesAdapter = ValuesAdapter()
        val linearLayout = LinearLayoutManager(context)
        recyclerView.setLayoutManager(linearLayout)
        recyclerView.setAdapter(valuesAdapter)
        variable?.values?.let { valuesAdapter!!.setData(it) }
    }
}// Required empty public constructor
