package ui.fragment.indicator


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import api.Constant
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.FragmentIndicatorBinding
import pojo.res.Variable


class IndicatorFragment : Fragment() {

    private var binding: FragmentIndicatorBinding? = null
    private var rootView: View? = null
    private var variable: Variable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
             variable = arguments?.getParcelable(Constant.VARIABLE);
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_indicator, container, false)
        rootView = binding!!.root

        binding!!.variable = variable

        return rootView
    }

}
