package ui.fragment.main

import api.RetrofitClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pojo.res.MyResponse

class MainFragmentModel : MainFragmentContract.Model {

    override suspend fun getData(): List<MyResponse> {
        return withContext(Dispatchers.IO) { RetrofitClient.api.getData() }
    }

}
