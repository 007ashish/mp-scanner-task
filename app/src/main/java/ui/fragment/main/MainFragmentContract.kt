package ui.fragment.main

import androidx.lifecycle.LiveData
import pojo.res.MyResponse

interface MainFragmentContract {

    interface View {
        fun toggleProgressBar(show: Boolean)
        fun initRecyclerView()
    }

    interface ViewModel {
        fun getResponse(): LiveData<List<MyResponse>>
    }

    interface Model {
       suspend fun getData(): List<MyResponse>
    }

}
