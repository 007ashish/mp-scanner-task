package ui.fragment.main


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.FragmentMainBinding
import ui.fragment.main.adapter.MainDataAdapter


class MainFragment : Fragment(), MainFragmentContract.View {

    lateinit var viewModel: MainFragmentViewModel
    lateinit var binding: FragmentMainBinding
    lateinit var rootView: View
    private var mainDataAdapter: MainDataAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        rootView = binding.root

        initRecyclerView()

        toggleProgressBar(true)

        viewModel.getResponse().observe(this, Observer {
            if (!it.isEmpty()) mainDataAdapter?.setData(it) else toggleProgressBar(false)
        })


        return rootView
    }


    override fun initRecyclerView() {
        mainDataAdapter = MainDataAdapter()
        val linearLayout = LinearLayoutManager(context)
        binding.rvData.layoutManager = linearLayout
        binding.rvData.adapter = mainDataAdapter
    }


    override fun toggleProgressBar(show: Boolean) {
        if (show)
            binding.progressBar.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
    }

    fun initView() {
        viewModel = ViewModelProviders.of(this).get(MainFragmentViewModel::class.java)
    }
}