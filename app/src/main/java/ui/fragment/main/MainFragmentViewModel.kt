package ui.fragment.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pojo.res.MyResponse

class MainFragmentViewModel : ViewModel(), MainFragmentContract.ViewModel {

    val model: MainFragmentModel

    init {
        model = MainFragmentModel()
    }


    val mutableResponseLiveData = MutableLiveData<List<MyResponse>>()

    override fun getResponse(): LiveData<List<MyResponse>> {
        viewModelScope.launch(Dispatchers.Main) {
            mutableResponseLiveData.value = model.getData()
        }
        return mutableResponseLiveData;
    }

    override fun onCleared() {
        super.onCleared()
    }

}
