package ui.fragment.main.adapter

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import api.Constant
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.MainListLayoutBinding
import pojo.res.MyResponse
import util.logError
import java.util.*

class MainDataAdapter : RecyclerView.Adapter<MainDataAdapter.MainDataViewHolder>() {

    private val mData = ArrayList<MyResponse>()
    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainDataViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding =
            DataBindingUtil.inflate<MainListLayoutBinding>(layoutInflater!!, R.layout.main_list_layout, parent, false)

        return MainDataViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainDataViewHolder, position: Int) {
        try {
            holder.binding.data = mData[position]
        } catch (e: Exception) {
            e.logError()
        }

    }

    override fun getItemCount(): Int {
        return mData.size
    }

    fun setData(value: List<MyResponse>) {
        this.mData.clear()
        this.mData.addAll(value)
        notifyDataSetChanged()
    }


    inner class MainDataViewHolder(var binding: MainListLayoutBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        init {
            this.binding.color = Color()
            binding.root.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            val bundle = Bundle()
            bundle.putParcelableArrayList(Constant.CRITERIA, mData[adapterPosition].criteria)
            bundle.putParcelable(Constant.CURRENT_DATA, mData[adapterPosition])
            Navigation.findNavController(view).navigate(R.id.destinationCriteria, bundle)
        }
    }
}
