package ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.marketpluse.scannertask.R
import com.marketpluse.scannertask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    /*   private lateinit var job: Job

       val handler = CoroutineExceptionHandler { _, e -> e.logError() }

       override val coroutineContext: CoroutineContext
           get() = Dispatchers.Main + job + handler*/

    lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this, R.id.nav_main_host_fragment)
    }


}
